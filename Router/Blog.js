const express = require("express");
const router = express.Router();
const isAuthenticated = require("../Middleware/Authentication");
const BlogValidation = require('../Middleware/BlogValidasi')
let blogposts = require("../FileJSON/blogs.json")


router.post("/blogpost", isAuthenticated, BlogValidation, (req, res) => {

    const GetID =(blogposts.length && blogposts[blogposts.length - 1].id) || 0;
    let id = GetID + 1;

    const { title, content, likes } = req.body;

    const blogpost = {
        id: id,
        email: req.userData.email,
        title:title,
        content:content,
        likes:likes,
    };

    // validasi duplikasi data

    const cekTitle = blogposts.find((blogpost) => blogpost.title == title);

    if (cekTitle) {
        return res.send({
            message: "Title Sudah Ada [Harus Unik]"
        });
    }

    blogposts.push(blogpost);

    return res.send({
        id: id,
        message: "blogpost berhasil ditambahkan",
        data: blogposts,
    });
});

//melihat blogspot Tanpa Middleware Login
router.get("/blogpost", (req, res) => {
    return res.send(blogposts);
});


//update likes blogspot Tanpa Middleware Login
router.put("/blogpost/likes/:id", (req, res) => {
    let blog = blogposts.find(i=>i.id==req.params.id);
    const params = { likes: blog.likes + 1};
    blog = {...blog,...params};
    
    blogposts = blogposts.map(i=> i.id == blog.id ? blog:i);

    return res.status(200).json(blog)
});


// Mengimpor router
module.exports = router