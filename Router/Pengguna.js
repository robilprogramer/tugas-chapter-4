
// Impor modul ekspres
const express = require("express")
const jwt = require("jsonwebtoken");
const router = express.Router()
let users = require("../FileJSON/users.json")
const validsiDaftar = require('../Middleware/RegisterValidasi')
//Menangani permintaan menggunakan route

router.post("/login", (req, res) => {

    const { email, password } = req.body;

    const isFoundUser = users.find((user) => user.email == email);

    if (isFoundUser) {
        const isValidPassword = isFoundUser.password == password;
        const namaLengkap = isFoundUser.namaLengkap;
        if (isValidPassword) {
            const jwtPayload = jwt.sign(
                {
                    id: isFoundUser.id,
                    email: isFoundUser.email,
                    namaLengkap: isFoundUser.namaLengkap
                }, "Robil12345!@#$5"
            );
            return res.json({
                token: jwtPayload,
                message: "Login Berhasil",
                NamaLengkap: namaLengkap
            });
        }
    }

    return res.status(400).json({
        error: true,
        message: "Login gagal, user tidak terdaftar atau password anda salah",
    });
});


router.post("/daftar", validsiDaftar, (req, res) => {
    const { email, password, namaLengkap, alamat, jenisKelamin } = req.body;
    //GET ID
    const GetID = (users.length && users[users.length - 1].id) || 0;
    let id = GetID + 1;

    const user = { id, email, password, namaLengkap, alamat, jenisKelamin };
    users.push(user);
    res.status(201).json(user);
});

// Mengimpor router
module.exports = router