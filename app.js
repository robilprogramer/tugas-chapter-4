const express = require("express")

// Mengimpor semua ruter
const PenggunaRouter = require("./Router/Pengguna")
const BlogRouter = require("./Router/Blog")

// Membuat server ekspres
const app = express()
// render
app.use(express.json());
// Menangani permintaan ruter

//Pengguna
app.use("/pengguna", PenggunaRouter)

//blog
app.use("/", BlogRouter)

app.listen((3000), () => {
    console.log("Server is Running")
})